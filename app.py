import boto3
from botocore.client import Config
from botocore.exceptions import ClientError
from datetime import datetime
import io
import logging
import os
import socket

UDP_IP = "0.0.0.0"
UDP_PORT = 5000
BUFFER_SIZE = 2048


def load_env(key, default=None):
    if key in os.environ:
        return os.environ.get(key)
    if default is not None:
        return default
    raise ValueError('Key \'{}\' is not set as environment variable!'.format(key))


if __name__ == '__main__':
    print("UDP listener is running")
    logEnabled = load_env("LOG_ENABLED", "true") == "true"
    s3Enabled = load_env("S3_ENABLED", "false") == "true"

    if logEnabled:
        print("Print out to log is enabled")
    if s3Enabled:
        print("S3 is enabled")

        s3uploader = boto3.resource(
            's3',
            endpoint_url=load_env("S3_ENDPOINT_URL"),
            aws_access_key_id=load_env("S3_ACCESS_KEY"),
            aws_secret_access_key=load_env("S3_SECRET_KEY"),
            config=Config(signature_version='s3v4'),
        )
        bucketName = load_env("S3_BUCKET_NAME")

    sock = socket.socket(socket.AF_INET,  # Internet
                         socket.SOCK_DGRAM)  # UDP
    sock.bind((UDP_IP, UDP_PORT))

    while True:
        data, addr = sock.recvfrom(BUFFER_SIZE)
        if logEnabled:
            print(f"{addr}: {data}")

        if s3Enabled:
            try:
                response = s3uploader.meta.client.upload_fileobj(
                    io.BytesIO(data),
                    bucketName,
                    str(datetime.now().strftime("%Y-%m-%d/%H-%M-%S_%f")))
            except ClientError as e:
                logging.error(e)
